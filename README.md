# [JIRA issue](https://github.com/mike1pol/atom-jira-issue)

Create, update JIRA issue from Atom

## Version 1.0.0
- [x] Login & password auth
- [ ] In progress list
- [ ] Search issue
- [ ] Last search issue queries
- [ ] JQL helper on search issue
- [ ] JIRA filters
- [ ] Create issue
- [ ] Update issue status
- [ ] Update issue work log
- [ ] Comment issue
