'use babel';

import axios from 'axios';

export default class API {
    constructor(api, auth) {
        this.api = api;
        this.auth = auth;
    }

    setAPI(api) {
        this.api = api;
    }

    setAuth(auth) {
        this.auth = auth;
    }

    client(method, path, data = {}) {
        const headers = {
            'Content-Type': 'application/json'
        };
        if (this.auth && this.auth.type === 'login') {
            const auth = new Buffer(`${this.auth.login}:${this.auth.password}`).toString('base64');
            headers.Authorization = `Basic ${auth}`;
        }
        return axios({
            url: `${this.api}/${path}`,
            method,
            data,
            headers
        });
    }

    suggestions(fieldName) {
        return this.client('get', `/rest/api/2/jql/autocompletedata/suggestions?fieldName=${fieldName}`);
    }

    autocomplete() {
        return this.client('get', '/rest/api/2/jql/autocompletedata');
    }

    searchIssue(jql) {
        return this.client('get', `/rest/api/2/search?jql=${jql}`);
    }
}
