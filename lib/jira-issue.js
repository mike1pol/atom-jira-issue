'use babel';
import {
    CompositeDisposable
} from 'atom';
import {
    $
} from 'space-pen';

import API from './api';
import IssueList from './view/issue-list';
const api = new API();

const error = err => {
    if (err.data && err.data.errorMessages) {
        atom.notifications.addError(err.data.errorMessages.join('<br>'));
    } else {
        atom.notifications.addError(err.toString());
    }
    console.error(err);
};

export default {
    config: {
        url: {
            type: 'string',
            title: 'JIRA Url',
            default: 'https://*.atlassian.net',
            order: 1
        },
        auth: {
            type: 'object',
            title: 'JIRA Authentication',
            order: 2,
            collapsed: true,
            properties: {
                type: {
                    type: 'string',
                    title: 'Type',
                    default: 'none',
                    order: 1,
                    enum: [
                        'none',
                        'login'
                    ]
                },
                login: {
                    type: 'string',
                    title: 'Login',
                    order: 2,
                    default: ''
                },
                password: {
                    type: 'string',
                    title: 'Password',
                    order: 3,
                    default: ''
                }
            }
        }
    },
    subscriptions: null,

    activate() {
        api.setAPI(atom.config.get('jira-issue.url'));
        api.setAuth(atom.config.get('jira-issue.auth'));
        atom.config.observe('jira-issue.url', newValue => {
            api.setAPI(newValue);
        });
        atom.config.observe('jira-issue.auth', newValue => {
            api.setAuth(newValue);
        });
        this.list = new IssueList();

        this.subscriptions = new CompositeDisposable();

        this.subscriptions.add(atom.commands.add('atom-workspace', {
            'jira-issue:inProgress': () => this.inProgress(),
            'jira-issue:create': () => this.create(),
            'jira-issue:search': () => this.search()
        }));
    },

    deactivate() {
        this.list.destroy();
        this.subscriptions.dispose();
    },

    inProgressSelect(item) {
        console.log('Selected item', item);
    },

    inProgress() {
        let searchString = 'status="In Progress"';
        const login = atom.config.get('jira-issue.auth').login;
        if (login && login.length > 0) {
            searchString += ` AND assignee=${login}`;
        }
        api.searchIssue(searchString)
            .then(res => {
                // const total = res.data.total;
                this.list.setList(res.data.issues.map(v => ({
                    id: v.key,
                    name: v.fields.summary,
                    description: (v.fields.description && v.fields.description.length > 0) ? v.fields.description.substring(0, 200) : '',
                    search: `${v.key} ${v.fields.summary} ${v.fields.description}`
                })));
                this.list.setCallback(this.inProgressSelect);
                this.list.getFilterKey = () => ('search');
                this.list.show();
                this.list.focusFilterEditor();
            })
            .catch(error);
    },

    create() {
        console.log('create');
    },

    searchSelect(item) {
        if (item.id === '> Search') {
            const searchString = this.getFilterQuery();
            this.list.empty();
            this.setLoading();
            api.searchIssue(searchString)
                .then(res => {
                    const list = res.data.issues.map(v => ({
                        id: v.key,
                        name: v.fields.summary,
                        description: (v.fields.description && v.fields.description.length > 0) ? v.fields.description.substring(0, 200) : '',
                        search: `${v.key} ${v.fields.summary} ${v.fields.description}`
                    }));
                    list.unshift({
                        id: '> Search'
                    });
                    list.forEach(item => {
                        const itemView = $(this.viewForItem(item));
                        itemView.data('select-list-item', item);
                        this.list.append(itemView);
                    });
                    this.selectItemView(this.list.find('li:first'));
                    this.setLoading();
                    this.list.getFilterKey = () => ('search');
                })
                .catch(err => {
                    const item = {
                        id: '> Search'
                    };
                    const itemView = $(this.viewForItem(item));
                    itemView.data('select-list-item', item);
                    this.list.append(itemView);
                    this.selectItemView(this.list.find('li:first'));
                    error(err);
                });
        } else {
            console.log('Item selected', item);
            if (this.populateListOld) {
                this.populateList = this.populateListOld;
                this.populateListOld = null;
            }
            if (this.cb) {
                this.cb(item);
                this.cb = null;
            }
            if (this.confirmedOld) {
                this.confirmed = this.confirmedOld;
                this.confirmedOld = null;
            }
            this.panel.hide();
        }
    },

    searchPopulate() {
    },

    search() {
        this.list.setList([
            {
                id: '> Search'
            }
        ]);
        this.list.setConfirmed(this.searchSelect);
        this.list.setPopulateList(this.searchPopulate);
        this.list.show();
        this.list.focusFilterEditor();
    }
};
