'use babel';
import {
    SelectListView
} from 'atom-space-pen-views';

export default class issueList extends SelectListView {
    constructor() {
        super();
        this.addClass('from-top');
        this.panel = atom.workspace.addModalPanel({
            item: this
        });
    }

    setCallback(cb) {
        this.cb = cb;
    }

    setList(list) {
        this.setItems(list);
    }

    setPopulateList(func) {
        this.populateListOld = this.populateList;
        this.populateList = func;
    }

    setConfirmed(func) {
        this.confirmedOld = this.confirmed;
        this.confirmed = func;
    }

    show() {
        this.panel.show();
    }

    viewForItem(item) {
        return `
        <li>
            <strong>${item.id}</strong>${(item.name) ? ` - ${item.name}` : ''}<br>
            ${(item.description) ? item.description : ''}
        </li>`;
    }

    confirmed(item) {
        if (this.populateListOld) {
            this.populateList = this.populateListOld;
            this.populateListOld = null;
        }
        if (this.cb) {
            this.cb(item);
            this.cb = null;
        }
        if (this.confirmedOld) {
            this.confirmed = this.confirmedOld;
            this.confirmedOld = null;
        }
        this.panel.hide();
    }

    cancelled() {
        if (this.populateListOld) {
            this.populateList = this.populateListOld;
            this.populateListOld = null;
        }
        if (this.cb) {
            this.cb(null);
            this.cb = null;
        }
        if (this.confirmedOld) {
            this.confirmed = this.confirmedOld;
            this.confirmedOld = null;
        }
        this.panel.hide();
    }
}
