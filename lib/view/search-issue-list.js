'use babel';
import {
    SelectListView
} from 'atom-space-pen-views'

export default class issueList extends SelectListView {
    constructor() {
        super();
        this.addClass('from-top');
        this.panel = atom.workspace.addModalPanel({
            item: this
        });
    }

    setCallback(cb) {
        this.cb = cb;
    }

    setList(list) {
        this.setItems(list);
    }

    show() {
        this.panel.show();
        this.focusFilterEditor();
    }

    viewForItem(item) {
        return `
        <li>
            <strong>${item.id}</strong> - ${item.name}<br>
            ${item.description}
        </li>`;
    }

    confirmed(item) {
        if (this.cb) {
            this.cb(item);
            this.cb = null;
        }
        this.panel.hide();
    }

    cancelled() {
        if (this.cb) {
            this.cb(null);
            this.cb = null;
        }
        this.panel.hide();
    }
}
